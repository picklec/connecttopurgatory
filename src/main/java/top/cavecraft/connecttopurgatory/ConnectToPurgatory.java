package top.cavecraft.connecttopurgatory;

import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;

public final class ConnectToPurgatory extends JavaPlugin {
    AsynchronousSocketChannel purgatorySocket;

    @Override
    public void onEnable() {
        try {
            purgatorySocket = AsynchronousSocketChannel.open();
            purgatorySocket.connect(new InetSocketAddress("127.0.0.1", 25564)).get();
            ByteBuffer portBuf = ByteBuffer.allocate(64);
            portBuf.putInt(getServer().getPort());
            purgatorySocket.write(portBuf);
            purgatorySocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
